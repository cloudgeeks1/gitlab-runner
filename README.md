# gitlab runners

- Docker Installation
- https://get.docker.com/
```
curl -fsSL https://get.docker.com -o get-docker.sh
sh get-docker.sh
```


- https://docs.gitlab.com/runner/install/linux-repository.html#installing-gitlab-runner

- Ubuntu
```installation
curl -L "https://packages.gitlab.com/install/repositories/runner/gitlab-runner/script.deb.sh" | sudo bash
```

sudo apt-get install gitlab-runner

systemctl status gitlab-runner

systemctl enable gitlab-runner

- registration
```register
gitlab-runner register

gitlab-runner status

gitlab-runner list

gitlab-runner verify --delete -t uLxMwdxdC1vmbXtceomu -u https://gitlab.com/

gitlab-runner verify

gitlab-runner --help
```

- permissions
```docker
chmod 666 /var/run/docker.sock
```

- gitlab cicd setup

- .gitlab-ci.yml

```gitlab-ci.yml
image: python:latest

before_script:
# Install
  - apt update -y
  - apt install -y jq

build:
  script:
    - docker-compose --project-name app up -d --build
```
- .gitlab-ci.yml
```gitlab-ci.yaml
image: docker:19.03.1

variables:
    DOCKERFILE_LOCATION: ./
    DOCKER_VERSION: Docker-19.03.6-ce
    DOCKER_TLS_CERTDIR: ""

services:
  - docker:19.03.1-dind

stages:
  - build
  - deploy


###############
#   Builds    #
###############

build-docker-latest:
  stage: build
  environment:
    name: dev
  script:
    - echo "AWSCLI Installation"
    - docker ps -a
    - docker-compose --version

  only:
    - main
   

###############
#   Deploy    #
###############

deploy-to-application-repo:
  stage: deploy
  environment:
    name: dev
  script:
    - echo "hello"
  only:
    - main

```

- docker-compose installation

```
# https://docs.docker.com/compose/cli-command/
# https://docs.docker.com/compose/profiles/
# https://github.com/EugenMayer/docker-image-atlassian-jira/blob/master/docker-compose.yml

#########################################################################################
# 1 Run the following command to download the current stable release of Docker Compose
#########################################################################################

 mkdir -p ~/.docker/cli-plugins/
 curl -SL https://github.com/docker/compose/releases/download/v2.0.1/docker-compose-linux-x86_64 -o ~/.docker/cli-plugins/docker-compose
 
 ###############################################
 # 2 Apply executable permissions to the binary
 ###############################################
 
  chmod +x ~/.docker/cli-plugins/docker-compose
  
  ###############################################
  # 3 Apply executable permissions to the binary
  ###############################################
  
  docker compose version
  
  
  
  
  # Commands
  # Build a Specific Profile
 #  docker compose -p app up -d --build

###########################
# Docker Compose Version 1
###########################
# https://docs.docker.com/compose/install/

curl -L "https://github.com/docker/compose/releases/download/1.29.2/docker-compose-$(uname -s)-$(uname -m)" -o /usr/local/bin/docker-compose
chmod +x /usr/local/bin/docker-compose
ln -s /usr/local/bin/docker-compose /usr/bin/docker-compose
docker-compose --version
```

- docker-compose commands
```docker-compose

docker-compose up -d --build

docker-compose --project-name app up -d --build
```

- crontab entry for automated cleaning of dangling images
```dangling_images
0 8 * * * yes | docker image prune
```

- docker socket permissions
```
#!/bin/bash
# Purpose: Set Docker Socket Permissions after reboot & Docker Logging

###########################
# Docker Socket Permissions
###########################
cat <<EOF > ${HOME}/docker-socket.sh
#!/bin/bash
chmod 666 /var/run/docker.sock
#End
EOF

chmod +x ${HOME}/docker-socket.sh

cat <<EOF > /etc/systemd/system/docker-socket.service
[Unit]
Description=Docker Socket Permissions
After=docker.service
BindsTo=docker.service
ReloadPropagatedFrom=docker.service

[Service]
Type=oneshot
ExecStart=${HOME}/docker-socket.sh
ExecReload=${HOME}/docker-socket.sh
RemainAfterExit=yes

[Install]
WantedBy=multi-user.target
EOF

systemctl start docker-socket.service

systemctl enable docker-socket.service

################
# Docker Logging
################

cat << EOF > /etc/docker/daemon.json
{
  "log-driver": "json-file",
  "log-opts": {
    "max-size": "1024m",
    "max-file": "5"
  }
}

EOF

shutdown -r now

# End
```
